let urlUsers = 'https://ajax.test-danit.com/api/json/users'
let urlPosts = 'https://ajax.test-danit.com/api/json/posts'
const addPostButton = document.querySelector('.add-post')
const modal = document.querySelector('.modal')
const closeModal = document.querySelector('.btn-secondary')
const closeModalCross = document.querySelector('.btn-close')
const saveModal = document.querySelector('.btn-primary')
const content = document.getElementById('content');
const loader = document.querySelector('.lds-roller')


class Card {
    constructor(post,user) {
      this.post = post;
      this.user = user
    }
  
    render() {
    const {id, title, body } = this.post;
    let{name,email} = this.user
    const cardElement = document.createElement('div');
    cardElement.classList.add('post');
    cardElement.innerHTML = `
        <div class="post__user">
            <span class="post__user--name">${name} </span><span class="post__user--email">${email}</span>
        </div>
        <div class="post__title">
            <p class="post__title--text">${title}</p>
        </div>
        <div class="post__body">
            <p class="post__body--text">${body}</p>
        </div>
        <button class="delete-btn" data-post-id="${id}">Delete</button>
        `;
    return cardElement;
    }
}

async function fetchPosts() {
    const usersResponse = await fetch(urlUsers);
    const postsResponse = await fetch(urlPosts);
    const users = await usersResponse.json();
    const posts = await postsResponse.json();
    return { users, posts };
}

function deletePost(postId) {
    return fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
      method: 'DELETE'
    });
}

async function displayContent() {
    const { users, posts } = await fetchPosts();
    addPostButton.style.display = 'flex'
    loader.style.display = 'none'
    posts.forEach(post => {
        const user = users[post.userId-1]
        const card = new Card(post, user);
        const cardElement = card.render();
            cardElement.querySelector('.delete-btn').addEventListener('click', async () => {
                const postId = cardElement.querySelector('.delete-btn').dataset.postId;
                await deletePost(postId);
                cardElement.remove();
            });
        content.appendChild(cardElement);
    });
}

addPostButton.addEventListener('click', (event)=>{
    modal.style.display = 'block'
})

closeModal.addEventListener('click',()=>{
    modal.style.display = 'none'
})

closeModalCross.addEventListener('click',()=>{
    modal.style.display = 'none'
})
let postTitle, postBody;
const addPostTitle = document.querySelector('.add-post-title')
const addPostBody = document.querySelector('.add-post-body')

let i = 100
saveModal.addEventListener('click', async (event)=>{
    loader.style.display = 'block'

    postTitle = addPostTitle.value
    postBody = addPostBody.value
    modal.style.display = 'none'
    const newPost = {
        id: i++,
        userId: 1,
        title: postTitle,
        body: postBody
    }
    await fetch(urlPosts, {
        method:'POST',
        headers: {
            "Content-Type": "application/json",
            },
        body: JSON.stringify(newPost)
        })
    const {users, posts} = await fetchPosts();
    const card = new Card(newPost, users[0]);
    const cardElement = card.render();
    cardElement.querySelector('.delete-btn').addEventListener('click', async () => {
        const postId = cardElement.querySelector('.delete-btn').dataset.postId;
        await deletePost(postId);
        cardElement.remove();
    });
    loader.style.display = 'none'
    content.prepend(cardElement);
})



displayContent();